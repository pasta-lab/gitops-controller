#!/bin/bash

set -euxo pipefail

COMMENT_MARKER="add new tools below"
TOOL_TO_ADD=$1

# adds the line below comment marker
append_line() {
  LINE_TO_ADD=$1
  FILENAME=$2

  LINE_NUMBER=$(grep -n "${COMMENT_MARKER}" "${FILENAME}" | cut -d':' -f1 | tail -n 1)

  # If the comment marker is not found in the file, exit with an error
  if [ -z "${LINE_NUMBER}" ]; then
    echo "Error: comment marker not found in file '${FILENAME}'"
    exit 1
  fi

  # Add the new line after the last occurrence of the comment marker in the file
  LINE_NUMBER=$((LINE_NUMBER+1))
  sed -i "${LINE_NUMBER}i\\
${LINE_TO_ADD}" "${FILENAME}"
}

# add tool in intall.sh file and import it in tools.go
append_line "go install ${TOOL_TO_ADD}" "install.sh"
append_line "\t_ \"${TOOL_TO_ADD}\"" "tools.go"

# add tool to go.mod
go get "${TOOL_TO_ADD}"
# tidy, so the tool become a direct dependency
go mod tidy

# install the binary to GOBIN
./install.sh

goimports -w tools.go
