module gitlab.com/joao-fontenele/echo/tools

go 1.20

require (
	golang.org/x/tools v0.8.0
	golang.org/x/vuln v0.0.0-20230407211851-ee3d87385065
)

require (
	github.com/google/go-cmp v0.5.9 // indirect
	golang.org/x/mod v0.10.0 // indirect
	golang.org/x/sync v0.1.0 // indirect
	golang.org/x/sys v0.7.0 // indirect
)
